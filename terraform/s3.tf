/* # TODO : Create a s3 bucket with aws_s3_bucket*/
resource "aws_s3_bucket" "b" {
  bucket = "s3-job-offer-bucket-fabre-lardon-jacob"
  force_destroy = "true"
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object
resource "aws_s3_object" "job_offers_raw_folder" {
  bucket = aws_s3_bucket.b.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "job_offers_raw_notification" {
  bucket = aws_s3_bucket.b.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "s3-job-offer-bucket-fabre-lardon-jacob/job_offers/raw/"
    filter_suffix       = ".csv"
  }
  depends_on = [aws_lambda_permission. allow_bucket]
}

